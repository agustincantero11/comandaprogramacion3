<?php

  use \Psr\Http\Message\ServerRequestInterface as Request;
  use \Psr\Http\Message\ResponseInterface as Response;

  require_once 'composer/vendor/autoload.php';

  require_once 'EmpleadoApi.php';
  require_once 'MesaApi.php';
  require_once 'MenuApi.php';
  require_once 'PedidoApi.php';
  require_once 'MWparaCORS.php';
  require_once 'MWparaAutentificar.php';
  require_once 'MWparaIncrementarOperacion.php';
  require_once 'AutentificadorJWT.php';
  require_once 'MWEmpleado.php';
  require_once 'Login.php';
  
  $config['displayErrorDetails'] = true;
  $config['addContentLengthHeader'] = false;

  $app = new \Slim\App(["settings" => $config]);
  
  $app->post('/login', \Login::class . ':loguear')->add(MWparaCORS::class . ':HabilitarCORSTodos')->add(MWparaCORS::class . ':HabilitarCORS8080');

  $app->group('/empleados', function () {
    
    $this->get('[/]', \EmpleadoApi::class . ':traerTodos');
    
    $this->get('/{id}', \EmpleadoApi::class . ':traerUno');
    
    $this->post('[/]', \EmpleadoApi::class . ':CargarUno')->add(\MWparaIncrementarOperacion::class . ':IncrementarOperacionAEmpleado');
    
    $this->delete('[/]', \EmpleadoApi::class . ':BorrarUno')->add(\MWparaIncrementarOperacion::class . ':IncrementarOperacionAEmpleado');
    
    $this->delete('/suspender', \EmpleadoApi::class . ':SuspenderEmpleado')->add(\MWparaIncrementarOperacion::class . ':IncrementarOperacionAEmpleado');
    
    $this->put('[/]', \EmpleadoApi::class . ':ModificarUno')->add(\MWparaIncrementarOperacion::class . ':IncrementarOperacionAEmpleado');
        
  })->add(MWparaAutentificar::class . ':VerificarUsuario')->add(MWparaCORS::class . ':HabilitarCORS8080')->add(\MWEmpleado::class . ':ValidarAdmin');


$app->get('/obtenerOperacionesPorPuesto', \EmpleadoApi::class . ':ObtenerOperacionesPorPuesto')->add(\MWEmpleado::class . ':ValidarAdmin')->add(MWparaAutentificar::class . ':VerificarUsuario');
$app->get('/obtenerOperacionesDeEmpleadosPorPuesto/{puesto}[/]', \EmpleadoApi::class . ':ObtenerOperacionesDeEmpleadosPorPuesto')->add(\MWEmpleado::class . ':ValidarAdmin')->add(MWparaAutentificar::class . ':VerificarUsuario');
$app->post('/listarEmpleadosEntreFechasLogin', \EmpleadoAPI::class . ':ListarEmpleadosEntreFechasLogin')->add(\MWEmpleado::class . ':ValidarAdmin')->add(MWparaAutentificar::class . ':VerificarUsuario');
$app->post('/listarEmpleadosEntreFechasRegistro', \EmpleadoAPI::class . ':ListarEmpleadosEntreFechasRegistro')->add(\MWEmpleado::class . ':ValidarAdmin')->add(MWparaAutentificar::class . ':VerificarUsuario');

$app->group('/mesas', function () {
    $this->get('[/]', \MesaApi::class . ':TraerTodas')->add(\MWEmpleado::class . ':ValidarAdmin');
    
    $this->get('/masUsada[/]', \MesaApi::class . ':MesaMasUsada')->add(\MWEmpleado::class . ':ValidarAdmin');
    
    $this->get('/menosUsada[/]', \MesaApi::class . ':MesaMenosUsada')->add(\MWEmpleado::class . ':ValidarAdmin');
    
    $this->get('/masFacturacion[/]', \MesaApi::class . ':MesaMasFacturacion')->add(\MWEmpleado::class . ':ValidarAdmin');
    
    $this->get('/menosFacturacion[/]', \MesaApi::class . ':MesaMenosFacturacion')->add(\MWEmpleado::class . ':ValidarAdmin');
    
    $this->get('/facturaConMasImporte[/]', \MesaApi::class . ':FacturaConMasImporte')->add(\MWEmpleado::class . ':ValidarAdmin');
    
    $this->get('/facturaConMenosImporte[/]', \MesaApi::class . ':FacturaConMenosImporte')->add(\MWEmpleado::class . ':ValidarAdmin');
    
    $this->get('/mejorPuntuacion[/]', \MesaApi::class . ':MesaConMejorPuntuacion')->add(\MWEmpleado::class . ':ValidarAdmin');
    
    $this->get('/peorPuntuacion[/]', \MesaApi::class . ':MesaConPeorPuntuacion')->add(\MWEmpleado::class . ':ValidarAdmin');
    
    $this->post('/facturacionEntreFechas[/]', \MesaApi::class . ':FacturacionEntreFechas')->add(\MWEmpleado::class . ':ValidarAdmin');
    
    $this->post('[/]', \MesaApi::class . ':CargarMesa')->add(\MWparaIncrementarOperacion::class . ':IncrementarOperacionAEmpleado')->add(\MWEmpleado::class . ':ValidarAdmin');
    $this->post('/foto', \MesaApi::class . ':SubirFotoMesa')->add(\MWparaIncrementarOperacion::class . ':IncrementarOperacionAEmpleado')->add(\MWEmpleado::class . ':ValidarMozo');
    $this->post('/esperandoPedido', \MesaApi::class . ':CambiarAEsperandoPedido')->add(\MWparaIncrementarOperacion::class . ':IncrementarOperacionAEmpleado')->add(\MWEmpleado::class . ':ValidarMozo');
    $this->post('/comiendo', \MesaApi::class . ':CambiarAComiendo')->add(\MWparaIncrementarOperacion::class . ':IncrementarOperacionAEmpleado')->add(\MWEmpleado::class . ':ValidarMozo');
    $this->post('/pagando', \MesaApi::class . ':CambiarAPagando')->add(\MWparaIncrementarOperacion::class . ':IncrementarOperacionAEmpleado')->add(\MWEmpleado::class . ':ValidarMozo');
    $this->post('/cerrarMesa', \MesaApi::class . ':CerrarMesa')->add(\MWparaIncrementarOperacion::class . ':IncrementarOperacionAEmpleado')->add(\MWEmpleado::class . ':ValidarMozo');
    $this->post('/cobrarMesa', \MesaApi::class . ':CobrarMesa')->add(\MWparaIncrementarOperacion::class . ':IncrementarOperacionAEmpleado')->add(\MWEmpleado::class . ':ValidarAdmin');
    $this->delete('/{codigo}', \MesaApi::class . ':BorrarMesa')->add(\MWparaIncrementarOperacion::class . ':IncrementarOperacionAEmpleado')->add(\MWEmpleado::class . ':ValidarAdmin');
})->add(MWparaAutentificar::class . ':VerificarUsuario');

$app->group('/pedidos', function () {
    
    $this->get('/traerPorMesa/{codigo}', \PedidoApi::class . ':TraerPorMesa');
    
    $this->get('/traerTodos[/]', \PedidoAPI::class . ':TraerTodosLosPedidos')->add(\MWEmpleado::class . ':ValidarAdmin');
    
    $this->get('/traerCancelados[/]', \PedidoApi::class . ':TraerTodosLosPedidosCancelados')->add(\MWEmpleado::class . ':ValidarAdmin');
    
    $this->post('/traerPorFecha[/]', \PedidoApi::class . ':TraerTodosLosPedidosPorFecha')->add(\MWEmpleado::class . ':ValidarAdmin');
    
    $this->post('[/]', \PedidoApi::class . ':CargarPedido')->add(\MWparaIncrementarOperacion::class . ':IncrementarOperacionAEmpleado')->add(\MWEmpleado::class . ':ValidarMozo');
    
    $this->delete('[/]', \PedidoApi::class . ':CancelarPedido')->add(\MWparaIncrementarOperacion::class . ':IncrementarOperacionAEmpleado')->add(\MWEmpleado::class . ':ValidarMozo');
    
    $this->post('/tomarPedido[/]', \PedidoApi::class . ':TomarPedidoPendiente')->add(\MWparaIncrementarOperacion::class . ':IncrementarOperacionAEmpleado');
    
    $this->post('/listoParaServir[/]', \PedidoApi::class . ':InformarPedidoListoParaServir')->add(\MWparaIncrementarOperacion::class . ':IncrementarOperacionAEmpleado');
    
    $this->post('/servir[/]', \PedidoApi::class . ':ServirPedido')->add(\MWparaIncrementarOperacion::class . ':IncrementarOperacionAEmpleado')->add(\MWEmpleado::class . ':ValidarMozo');
    
    $this->get('/tiempoRestante/{codigoPedido}[/]', \PedidoAPI::class . ':TiempoRestantePedido');
    
    $this->get('/listarFueraDelTiempoEstipulado[/]', \PedidoAPI::class . ':ListarPedidosFueraDelTiempoEstipulado')->add(\MWEmpleado::class . ':ValidarAdmin');
    
    $this->get('/masVendido[/]', \PedidoAPI::class . ':LoMasVendido')->add(\MWEmpleado::class . ':ValidarAdmin');
    
    $this->get('/menosVendido[/]', \PedidoAPI::class . ':LoMenosVendido')->add(\MWEmpleado::class . ':ValidarAdmin');
    
    
})->add(MWparaAutentificar::class . ':VerificarUsuario');

$app->group('/menu', function () {
    
    $this->get('[/]', \MenuApi::class . ':MostrarMenu');
    
    $this->post('[/]', \MenuApi::class . ':CargarMenu')->add(\MWparaIncrementarOperacion::class . ':IncrementarOperacionAEmpleado')->add(\MWEmpleado::class . ':ValidarAdmin');
    
    $this->put('[/]', \MenuApi::class . ':ModificarMenu')->add(\MWparaIncrementarOperacion::class . ':IncrementarOperacionAEmpleado')->add(\MWEmpleado::class . ':ValidarAdmin');
    
    $this->delete('[/]', \MenuApi::class . ':BorrarMenu')->add(\MWparaIncrementarOperacion::class . ':IncrementarOperacionAEmpleado')->add(\MWEmpleado::class . ':ValidarAdmin');
    
})->add(MWparaAutentificar::class . ':VerificarUsuario');


$app->run();
?>