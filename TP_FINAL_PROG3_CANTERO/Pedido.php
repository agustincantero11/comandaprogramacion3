<?php

include_once 'AccesoDatos.php';

class Pedido
{
   
    public $codigo;
    public $estado;
    public $mesa;
    public $descripcion;
    public $id_menu;
    public $sector;
    public $nombre_cliente;
    public $nombre_mozo;
    public $id_mozo;
    public $id_encargado;
    public $hora_inicial;
    public $hora_entrega_estimada;
    public $hora_entrega_real;
    public $fecha;
    public $importe;
    
    public static function ObtenerPorCodigo($codigo)
    {
        try {
            $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso();

            $consulta = $objetoAccesoDato->RetornarConsulta("SELECT p.codigo, ep.descripcion as estado, p.id_mesa as mesa, me.nombre as descripcion, p.id_menu=1, CASE
    WHEN me.id_sector=1 THEN 'Bartender'
    WHEN me.id_sector=2 THEN 'Cervecero'
    ELSE 'Cocinero'
END as puesto, p.nombre_cliente, em.nombre as nombre_mozo, p.id_mozo, p.id_encargado, p.hora_inicial, p.hora_entrega_estimada, p.hora_entrega_real, p.fecha, me.precio as importe FROM pedido p 
INNER JOIN estado_pedidos ep ON ep.id_estado_pedidos = p.id_estado_pedidos 
INNER JOIN menu me ON me.id = p.id_menu 
INNER JOIN empleados em ON em.id = p.id_mozo 
WHERE p.codigo = :codigo");
            
            $consulta->bindValue(':codigo', $codigo, PDO::PARAM_STR);
            $consulta->execute();

            $resultado = $consulta->fetchAll(PDO::FETCH_CLASS, "Pedido");
        } catch (Exception $e) {
            $resultado = $e->getMessage();
        }
        finally {
            return $resultado;
        }
    }
    
    public static function Cargar($id_mesa, $id_menu, $id_mozo, $nombre_cliente)
    {
        $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso();
        try {
            $consulta = $objetoAccesoDato->RetornarConsulta("SELECT Count(*) FROM menu m, mesa me, empleados em
WHERE m.id = :id_menu AND em.id = :id_mozo AND me.codigo_mesa = :id_mesa AND em.suspendido = '0' AND em.puesto = 'Mozo'");

            $consulta->bindValue(':id_menu', $id_menu, PDO::PARAM_INT);
            $consulta->bindValue(':id_mozo', $id_mozo, PDO::PARAM_INT);
            $consulta->bindValue(':id_mesa', $id_mesa, PDO::PARAM_STR);
            $consulta->execute();
            $validacion = $consulta->fetch();
            
            if ($validacion[0] > 0) {
                $codigo = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);

                date_default_timezone_set("America/Argentina/Buenos_Aires");
                $fecha = date('Y-m-d');
                $hora_inicial = date('H:i');

                $consulta = $objetoAccesoDato->RetornarConsulta("INSERT INTO pedido (codigo, id_estado_pedidos, fecha, hora_inicial, 
                                                                id_mesa, id_menu, id_mozo, nombre_cliente) 
                                                                VALUES (:codigo, 1, :fecha, :hora_inicial, 
                                                                :id_mesa, :id_menu, :id_mozo, :nombre_cliente);");

                $consulta->bindValue(':id_menu', $id_menu, PDO::PARAM_INT);
                $consulta->bindValue(':id_mozo', $id_mozo, PDO::PARAM_INT);
                $consulta->bindValue(':id_mesa', $id_mesa, PDO::PARAM_STR);
                $consulta->bindValue(':nombre_cliente', $nombre_cliente, PDO::PARAM_STR);
                $consulta->bindValue(':fecha', $fecha, PDO::PARAM_STR);
                $consulta->bindValue(':hora_inicial', $hora_inicial, PDO::PARAM_STR);
                $consulta->bindValue(':codigo', $codigo, PDO::PARAM_STR);
                $consulta->execute();

                Mesa::CambiarAEsperandoPedido($id_mesa);
                $respuesta = "Pedido registrado correctamente con codigo: ".$codigo;
            } else {
                $respuesta = "No se pudo registrar el pedido, el id de la mesa o el menu no existe: "."id_mesa: ".$id_mesa." - id_menu: ".$id_menu." - cliente: ".$nombre_cliente;
            }
        } catch (Exception $e) {
            $respuesta = $e->getMessage();
        }
        finally {
            return $respuesta;
        }
    }
    
    public static function TraerPorMesa($mesa)
    {
       try {
            $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso();

            $consulta = $objetoAccesoDato->RetornarConsulta("SELECT p.codigo, ep.descripcion as estado, p.id_mesa as mesa, me.nombre as descripcion, p.id_menu, em.puesto, p.nombre_cliente, em.nombre as nombre_mozo, p.id_mozo, p.id_encargado, p.hora_inicial, p.hora_entrega_estimada, p.hora_entrega_real, p.fecha, me.precio as importe FROM pedido p INNER JOIN estado_pedidos ep ON ep.id_estado_pedidos = p.id_estado_pedidos INNER JOIN menu me ON me.id = p.id_menu INNER JOIN empleados em ON em.id = p.id_mozo WHERE p.id_mesa = :mesa AND ep.descripcion NOT IN ('Cancelado','Finalizado')");
            $consulta->bindValue(':mesa', $mesa, PDO::PARAM_STR);
            $consulta->execute();

            $resultado = $consulta->fetchAll(PDO::FETCH_CLASS, "Pedido");
        } catch (Exception $e) {
            $mensaje = $e->getMessage();
            $resultado = array("Estado" => "ERROR", "Mensaje" => "$mensaje");
        }
        finally {
            return $resultado;
        }
    }

    public static function Finalizar($codigoMesa)
    {
        try {
            $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso();

            $consulta = $objetoAccesoDato->RetornarConsulta("UPDATE pedido SET id_estado_pedidos = 6 
                                                            WHERE id_estado_pedidos <> 5 AND id_mesa = :codigo");

            $consulta->bindValue(':codigo', $codigoMesa, PDO::PARAM_STR);

            $consulta->execute();

            $respuesta = "Pedidos de la mesa finalizados correctamente.";
        } catch (Exception $e) {
            $respuesta = $e->getMessage();
        }
        finally {
            return $respuesta;
        }
    }
    
    public static function Cancelar($codigo)
    {
        try {
            $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso();

            $consulta = $objetoAccesoDato->RetornarConsulta("UPDATE pedido SET id_estado_pedidos = 5 WHERE codigo = :codigo");

            $consulta->bindValue(':codigo', $codigo, PDO::PARAM_STR);

            $consulta->execute();

            $respuesta = "Pedido cancelado correctamente.";
        } catch (Exception $e) {
            $respuesta = $e->getMessage();
        }
        finally {
            return $respuesta;
        }
    }
    
    public static function TraerTodos()
    {
        try {
            $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso();

            $consulta = $objetoAccesoDato->RetornarConsulta("SELECT p.codigo, ep.descripcion as estado, p.id_mesa as mesa, 
                                                        me.nombre as descripcion, p.id_menu, em.puesto, p.nombre_cliente,
                                                        em.nombre as nombre_mozo, p.id_mozo, p.id_encargado, p.hora_inicial, p.hora_entrega_estimada,
                                                        p.hora_entrega_real, p.fecha, me.precio as importe
                                                        FROM pedido p
                                                        INNER JOIN estado_pedidos ep ON ep.id_estado_pedidos = p.id_estado_pedidos
                                                        INNER JOIN menu me ON me.id = p.id_menu
                                                        INNER JOIN empleados em ON em.id = p.id_mozo");

            $consulta->execute();

            $resultado = $consulta->fetchAll(PDO::FETCH_CLASS, "Pedido");
        } catch (Exception $e) {
            $resultado = $e->getMessage();
        }
        finally {
            return $resultado;
        }
    }
    
    public static function TraerCancelados()
    {
        try {
            $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso();

            $consulta = $objetoAccesoDato->RetornarConsulta("SELECT p.codigo, ep.descripcion as estado, p.id_mesa as mesa, 
                                                        me.nombre as descripcion, p.id_menu, em.puesto, p.nombre_cliente,
                                                        em.nombre as nombre_mozo, p.id_mozo, p.id_encargado, p.hora_inicial, p.hora_entrega_estimada,
                                                        p.hora_entrega_real, p.fecha, me.precio as importe
                                                        FROM pedido p
                                                        INNER JOIN estado_pedidos ep ON ep.id_estado_pedidos = p.id_estado_pedidos
                                                        INNER JOIN menu me ON me.id = p.id_menu
                                                        INNER JOIN empleados em ON em.id = p.id_mozo
                                                        WHERE ep.descripcion = 'Cancelado'");
            $consulta->execute();

            $resultado = $consulta->fetchAll(PDO::FETCH_CLASS, "Pedido");
        } catch (Exception $e) {
            $resultado = $e->getMessage();
        }
        finally {
            return $resultado;
        }
    }
    
    public static function TraerTodosPorFecha($fecha)
    {
        try {
            $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso();

            $consulta = $objetoAccesoDato->RetornarConsulta("SELECT p.codigo, ep.descripcion as estado, p.id_mesa as mesa, 
                                                        me.nombre as descripcion, p.id_menu, em.puesto, p.nombre_cliente,
                                                        em.nombre as nombre_mozo, p.id_mozo, p.id_encargado, p.hora_inicial, p.hora_entrega_estimada,
                                                        p.hora_entrega_real, p.fecha, me.precio as importe
                                                        FROM pedido p
                                                        INNER JOIN estado_pedidos ep ON ep.id_estado_pedidos = p.id_estado_pedidos
                                                        INNER JOIN menu me ON me.id = p.id_menu
                                                        INNER JOIN empleados em ON em.id = p.id_mozo
                                                        WHERE p.fecha = :fecha");
            $consulta->bindValue(':fecha', $fecha, PDO::PARAM_STR);
            $consulta->execute();

            $resultado = $consulta->fetchAll(PDO::FETCH_CLASS, "Pedido");
        } catch (Exception $e) {
            $mensaje = $e->getMessage();
            $resultado = array("Estado" => "ERROR", "Mensaje" => "$mensaje");
        }
        finally {
            return $resultado;
        }
    }
    
    public static function TomarPedido($codigo, $id_encargado, $minutosEstimadosDePreparacion, $puesto)
    {
        $pedido = Pedido::ObtenerPorCodigo($codigo);
        
        if ($pedido == null) {
            $respuesta = "Codigo incorrecto.";
        } else if ($pedido[0]->estado != 'Pendiente') {
            $respuesta = "Este pedido no se encuentra pendiente.";
        } else if (strcasecmp($pedido[0]->puesto, $puesto) != 0) {
            
            $respuesta = "Este pedido pertenece a otro sector.";
        } else{
            try {
                $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso();
    
                $time = new DateTime('now',new DateTimeZone('America/Argentina/Buenos_Aires'));
                $time->add(new DateInterval('PT' . $minutosEstimadosDePreparacion . 'M'));
    
                $hora_entrega_estimada = $time->format('H:i');
    
                $consulta = $objetoAccesoDato->RetornarConsulta("UPDATE pedido SET id_estado_pedidos = 2, id_encargado = :id_encargado, 
                                                                hora_entrega_estimada = :hora_entrega_estimada WHERE codigo = :codigo");
    
                $consulta->bindValue(':codigo', $codigo, PDO::PARAM_STR);
                $consulta->bindValue(':hora_entrega_estimada', $hora_entrega_estimada, PDO::PARAM_STR);
                $consulta->bindValue(':id_encargado', $id_encargado, PDO::PARAM_INT);
    
                $consulta->execute();
    
                $respuesta = "Pedido tomado correctamente.";
            } catch (Exception $e) {
                $mensaje = $e->getMessage();
                $respuesta = array("Estado" => "ERROR", "Mensaje" => "$mensaje");
            }
            finally {
                return $respuesta;
            }
            
        }
        
        return $respuesta;
    }
    
    public static function InformarListoParaServir($codigo, $idEncargado)
    {
        
        $pedido = Pedido::ObtenerPorCodigo($codigo); 
        
        if ($pedido == null) {
            $respuesta = "Codigo incorrecto.";
        } else if ($pedido[0]->estado != 'En Preparacion') {
            $respuesta = "Este pedido no se encuentra en preparacion.";
        } else if ($pedido[0]->id_encargado != $idEncargado) {
            $respuesta = "Solo el encargado del pedido puede realizar esta accion.";
        }else{
            try {
            $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso();

            $time = new DateTime('now',new DateTimeZone('America/Argentina/Buenos_Aires'));
            $hora_entrega_real = $time->format('H:i');

            $consulta = $objetoAccesoDato->RetornarConsulta("UPDATE pedido SET id_estado_pedidos = 3, hora_entrega_real = :hora_entrega_real 
                                                            WHERE codigo = :codigo");

            $consulta->bindValue(':codigo', $codigo, PDO::PARAM_STR);
            $consulta->bindValue(':hora_entrega_real', $hora_entrega_real, PDO::PARAM_STR);

            $consulta->execute();

            $respuesta = "Pedido listo para servir.";
            } catch (Exception $e) {
                $respuesta =  $e->getMessage();
            }
            finally {
                return $respuesta;
            }
        }
        return $respuesta;
    }
    
    public static function Servir($codigo, $idEncargado)
    {
        $pedido = Pedido::ObtenerPorCodigo($codigo);
        $mesa = $pedido[0]->mesa;
        if ($pedido == null) {
            $respuesta = "Codigo incorrecto.";
        } else if ($pedido[0]->estado != 'Listo para Servir') {
            $respuesta = "Este pedido no se encuentra listo para servir.";
        } else if ($pedido[0]->id_mozo != $idEncargado) {
            $respuesta = "Solo el mozo encargado del pedido puede realizar esta accion. Mozo: ".$idEncargado;
        } else {
            try {
            $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso();

            $consulta = $objetoAccesoDato->RetornarConsulta("UPDATE pedido SET id_estado_pedidos = 4 
                                                            WHERE codigo = :codigo");

            $consulta->bindValue(':codigo', $codigo, PDO::PARAM_STR);
            
            $consulta->execute();
            
            Mesa::CambiarAComiendo($mesa);
            
            $respuesta = "Pedido servido correctamente.";
            } catch (Exception $e) {
                $respuesta =  $e->getMessage();
            }
            finally {
                return $respuesta;
            }
        }
        return $respuesta;
    }
    
    public static function TiempoRestante($codigo)
    {
       $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso();

        $consulta = $objetoAccesoDato->RetornarConsulta("SELECT p.hora_entrega_estimada, ep.descripcion as estado FROM pedido p INNER JOIN estado_pedidos ep ON ep.id_estado_pedidos = p.id_estado_pedidos WHERE p.codigo = :codigo");

        $consulta->bindValue(':codigo', $codigo, PDO::PARAM_STR);
        $consulta->execute();
        $pedido = $consulta->fetch();
        
        if($pedido["estado"] == 'En Preparacion'){
            $time = new DateTime('now',new DateTimeZone('America/Argentina/Buenos_Aires'));
            $hora_entrega = new DateTime($pedido["hora_entrega_estimada"],new DateTimeZone('America/Argentina/Buenos_Aires'));
            
            if($time > $hora_entrega){
                $intervalo = $time->diff($hora_entrega);
                $respuesta = "Pedido retrasado ".$intervalo->format('%H:%I:%S');
            }else{
                $intervalo = $time->diff($hora_entrega);
                $respuesta = $intervalo->format('%H:%I:%S');
                return $respuesta;
            }                
        }else{
            $respuesta = "El pedido se encuentra ".$pedido["estado"];
        } 
        
        return $respuesta;
    }
    
    public static function ListarFueraDelTiempoEstipulado()
    {
        try {
            $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso();

            $consulta = $objetoAccesoDato->RetornarConsulta("SELECT p.codigo, ep.descripcion as estado, p.id_mesa as mesa, 
                                                        me.nombre as descripcion, p.id_menu, em.puesto, p.nombre_cliente,
                                                        em.nombre as nombre_mozo, p.id_mozo, p.id_encargado, p.hora_inicial, p.hora_entrega_estimada,
                                                        p.hora_entrega_real, p.fecha, me.precio as importe
                                                        FROM pedido p
                                                        INNER JOIN estado_pedidos ep ON ep.id_estado_pedidos = p.id_estado_pedidos
                                                        INNER JOIN menu me ON me.id = p.id_menu
                                                        INNER JOIN empleados em ON em.id = p.id_mozo
                                                        WHERE p.hora_entrega_estimada < p.hora_entrega_real");

            $consulta->execute();

            $respuesta = $consulta->fetchAll(PDO::FETCH_CLASS, "Pedido");
        } catch (Exception $e) {
            $respuesta = $e->getMessage();
        }
        finally {
            return $respuesta;
        }
    }
    
    public static function MasVendido()
    {
        try {
            $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso();

            $consulta = $objetoAccesoDato->RetornarConsulta("SELECT p.id_menu, m.nombre, count(p.id_menu) as cantidad_ventas FROM pedido p INNER JOIN menu m
                                                            on m.id = p.id_menu GROUP BY(id_menu) HAVING count(p.id_menu) = 
                                                            (SELECT MAX(sel.cantidad_ventas) FROM 
                                                            (SELECT count(p.id_menu) as cantidad_ventas FROM pedido p GROUP BY(id_menu)) sel);");

            $consulta->execute();

            $respuesta = $consulta->fetchAll();
        } catch (Exception $e) {
            $mensaje = $e->getMessage();
            $respuesta = array("Estado" => "ERROR", "Mensaje" => "$mensaje");
        }
        finally {
            return $respuesta;
        }
    }
    
    public static function MenosVendido()
    {
        try {
            $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso();

            $consulta = $objetoAccesoDato->RetornarConsulta("SELECT p.id_menu, m.nombre, count(p.id_menu) as cantidad_ventas FROM pedido p INNER JOIN menu m
                                                            on m.id = p.id_menu GROUP BY(id_menu) HAVING count(p.id_menu) = 
                                                            (SELECT MIN(sel.cantidad_ventas) FROM 
                                                            (SELECT count(p.id_menu) as cantidad_ventas FROM pedido p GROUP BY(id_menu)) sel);");

            $consulta->execute();

            $respuesta = $consulta->fetchAll();
        } catch (Exception $e) {
            $mensaje = $e->getMessage();
            $respuesta = array("Estado" => "ERROR", "Mensaje" => "$mensaje");
        }
        finally {
            return $respuesta;
        }
    }
}
?>