<?php

include_once 'AccesoDatos.php';

class Empleado{

	public $id;
    public $nombre;
    public $apellido;
    public $puesto;
    public $suspendido;
    public $email;
    public $password;
    public $fecha_registro;
    
    public function setNombre($nombre){
        $this->nombre = $nombre;
    }

    public function getNombre(){
        return $nombre;
    }

    public function setApellido($apellido){
        $this->apellido = $apellido;
    }

    public function getApellido(){
        return $apellido;
    }

    public function setPuesto($puesto){
        $this->puesto = $puesto;
    }

    public function getPuesto(){
        return $puesto;
    }

    public function setSuspendido($suspendido){
        $this->suspendido = $suspendido;
    }

    public function getSuspendido(){
        return $suspendido;
    }
    
    public function setEmail($email){
        $this->email = $email;
    }

    public function getEmail(){
        return $email;
    }
    
    public function setPassword($password){
        $this->password = $password;
    }

    public function getPassword(){
        return $password;
    }

    public function setFecha_registro($fecha_registro){
        $this->fecha_registro = $fecha_registro;
    }

    public function getFecha_registro(){
        return $fecha_registro;
    }
    
    public function InsertarElEmpleado()
	{
		date_default_timezone_set("America/Argentina/Buenos_Aires");
		$ahora = date('Y-m-d H:i:s');
		
		$puestos = array("Bartender","bartender", "Cervecero", "cervecero", "Cocinero", "cocinero", "Mozo", "mozo", "Administrador", "administrador");
		if (in_array($this->puesto, $puestos))
		{
			$objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso(); 
			$consulta =$objetoAccesoDato->RetornarConsulta("INSERT into empleados (nombre,apellido,puesto,email,password,fecha_registro)values(:nombre,:apellido,:puesto,:email,:password,:fecha_registro)");
			$consulta->bindValue(':nombre',$this->nombre, PDO::PARAM_STR);
			$consulta->bindValue(':apellido', $this->apellido, PDO::PARAM_STR);
			$consulta->bindValue(':puesto', $this->puesto, PDO::PARAM_STR);
			$consulta->bindValue(':email', $this->email, PDO::PARAM_STR);
			$consulta->bindValue(':password', $this->password, PDO::PARAM_STR);
			$consulta->bindValue(':fecha_registro', $ahora, PDO::PARAM_STR);
			
			$consulta->execute();		
			
			return "Registro exitoso, id: ".$objetoAccesoDato->RetornarUltimoIdInsertado();
		}
		
		return "El puesto ingresado no es valido";
    }
     
    public function BorrarEmpleado()
	{
	 		$objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso(); 
			$consulta =$objetoAccesoDato->RetornarConsulta("
				delete 
				from empleados 				
				WHERE id=:id");	
				$consulta->bindValue(':id',$this->id, PDO::PARAM_INT);		
				$consulta->execute();
				return $consulta->rowCount();
	}

    public function Suspender()
    {
		$objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso();

            $consulta = $objetoAccesoDato->RetornarConsulta("UPDATE empleados SET suspendido = 1 WHERE id=:id");

            $consulta->bindValue(':id',$this->id, PDO::PARAM_INT);

            $consulta->execute();

            return $consulta->rowCount();
    }
    
	public function ModificarEmpleado($id)
	{
	    $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso(); 
		$consulta =$objetoAccesoDato->RetornarConsulta("
				update empleados 
				set nombre=:nombre,
				apellido=:apellido,
                puesto=:puesto,
                suspendido=:suspendido,
                email=:email,
                password=:password
				WHERE id=:id");
		$consulta->bindValue(':id',$id, PDO::PARAM_INT);
		$consulta->bindValue(':nombre',$this->nombre, PDO::PARAM_INT);
		$consulta->bindValue(':apellido', $this->apellido, PDO::PARAM_STR);
        $consulta->bindValue(':puesto', $this->puesto, PDO::PARAM_STR);
		$consulta->bindValue(':suspendido', $this->suspendido, PDO::PARAM_STR);
		$consulta->bindValue(':email', $this->email, PDO::PARAM_STR);
		$consulta->bindValue(':password', $this->password, PDO::PARAM_STR);
		$consulta->execute();
		return $consulta->rowCount();
	}

  	public static function TraerTodosLosEmpleados()
	{
			$objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso(); 
            $consulta =$objetoAccesoDato->RetornarConsulta("select id, nombre, apellido, puesto, suspendido, email, password from empleados");
			$consulta->execute();			
			return $consulta->fetchAll(PDO::FETCH_CLASS, "Empleado");		
	}

	public static function TraerUnEmpleado($id) 
	{
			$objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso(); 
			$consulta =$objetoAccesoDato->RetornarConsulta("select id, nombre, apellido, puesto, password from empleados where id = $id");
			$consulta->execute();
			$empleado= $consulta->fetchObject('Empleado');
			return $empleado;				

			
	}
	
	public static function TraerPorEmail($email) 
	{
	    $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso(); 
			$consulta =$objetoAccesoDato->RetornarConsulta("select id, nombre, apellido, puesto, suspendido, email, password from empleados WHERE email=:email");
			$consulta->bindValue(':email',$email, PDO::PARAM_INT);
			$consulta->execute();
			$empleado= $consulta->fetchObject('Empleado');
			return $empleado;	
			
	}

	public static function ActualizarFechaLogin($id)
	{
		$objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso();

        date_default_timezone_set("America/Argentina/Buenos_Aires");
        $fecha = date('Y-m-d H:i:s');

        $consulta = $objetoAccesoDato->RetornarConsulta("UPDATE empleados SET fecha_ultimo_login = :fecha WHERE ID = :id");

        $consulta->bindValue(':fecha', $fecha, PDO::PARAM_STR);
        $consulta->bindValue(':id', $id, PDO::PARAM_INT);

        $consulta->execute();
	}
	
	public static function IncrementarOperacion($id)
	{
	    $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso();
        $consulta = $objetoAccesoDato->RetornarConsulta("UPDATE empleados 
                                                        SET cantidad_operaciones = cantidad_operaciones + 1
                                                        WHERE id = :id");
        $consulta->bindValue(':id', $id, PDO::PARAM_INT);
        $consulta->execute();
        $respuesta = "Se incremento la cantidad de operaciones correctamente.";
        return $respuesta;
	}
	
	public static function OperacionesPorPuesto()
    {
        $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso();

        $consulta = $objetoAccesoDato->RetornarConsulta("SELECT puesto, SUM(cantidad_operaciones) as cantidad_operaciones FROM empleados GROUP BY(puesto)");

        $consulta->execute();

        return $consulta->fetchAll();
    }
    
    public static function OperacionesDeEmpleadosPorPuesto($puesto)
    {
        $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso();

        $consulta = $objetoAccesoDato->RetornarConsulta("SELECT id, nombre, puesto, cantidad_operaciones FROM empleados WHERE puesto = :puesto");
		$consulta->bindValue(':puesto', $puesto, PDO::PARAM_STR);
        $consulta->execute();

        return $consulta->fetchAll();
    }
    
    public static function ListarEntreFechasLogin($fecha1,$fecha2)
    {
        $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso();

        $consulta = $objetoAccesoDato->RetornarConsulta("SELECT id, puesto, nombre, email, fecha_registro, fecha_ultimo_login,CASE WHEN suspendido = 0 THEN 'Habilitado' ELSE 'Suspendido' END AS Estado, cantidad_operaciones FROM empleados WHERE fecha_ultimo_login BETWEEN :fecha1 AND :fecha2");
        $consulta->bindValue(':fecha1', $fecha1, PDO::PARAM_STR);
        $consulta->bindValue(':fecha2', $fecha2, PDO::PARAM_STR);
        $consulta->execute();

        return $consulta->fetchAll(PDO::FETCH_CLASS, "Empleado");
    }
    
    public static function ListarEntreFechasRegistro($fecha1,$fecha2)
    {
        $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso();

        $consulta = $objetoAccesoDato->RetornarConsulta("SELECT id, puesto, nombre, email, fecha_registro, fecha_ultimo_login,CASE WHEN suspendido = 0 THEN 'Habilitado' ELSE 'Suspendido' END AS Estado, cantidad_operaciones FROM empleados WHERE fecha_registro BETWEEN :fecha1 AND :fecha2");
        $consulta->bindValue(':fecha1', $fecha1, PDO::PARAM_STR);
        $consulta->bindValue(':fecha2', $fecha2, PDO::PARAM_STR);
        $consulta->execute();

        return $consulta->fetchAll(PDO::FETCH_CLASS, "Empleado");
    }
}


?>