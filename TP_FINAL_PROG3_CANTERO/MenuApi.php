<?php
include_once("Menu.php");

class MenuApi extends Menu{  
    
    public function MostrarMenu($request,$response,$args){
        $respuesta = Menu::Mostrar();
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function CargarMenu($request, $response, $args){
        $parametros = $request->getParsedBody();
        $nombre = $parametros["nombre"];  
        $precio = $parametros["precio"];  
        $sector = $parametros["sector"];            

        $respuesta = Menu::Cargar($nombre,$precio,$sector);
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function ModificarMenu($request, $response, $args){
        $parametros = $request->getParsedBody();
        $id = $parametros["id"]; 
        $nombre = $parametros["nombre"];  
        $precio = $parametros["precio"];  
        $sector = $parametros["sector"];            

        $respuesta = Menu::Modificar($id,$nombre,$precio,$sector);
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function BorrarMenu($request,$response,$args){
        $parametros = $request->getParsedBody();
        $id = $parametros["id"]; 
        $respuesta = Menu::Borrar($id);
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
}