<?php
    class MWEmpleado{
        
        public static function ValidarAdmin($request,$response,$next){
            $arrayConToken = $request->getHeader('token');
            $token=$arrayConToken[0];
            $payload = AutentificadorJWT::ObtenerPayLoad($token);
            $puesto = $payload->datos->puesto;
            if($puesto == "Administrador" || $puesto == "administrador"){
                return $next($request,$response);
            }
            else{
                $respuesta = "No tienes permiso para realizar esta accion.";
                $newResponse = $response->withJson($respuesta,200);
                return $newResponse;
            }
        }
        
        public static function ValidarMozo($request,$response,$next){
            $arrayConToken = $request->getHeader('token');
            $token=$arrayConToken[0];
            $payload = AutentificadorJWT::ObtenerPayLoad($token);
            $puesto = $payload->datos->puesto;
            if($puesto == "Mozo" || $puesto == "mozo"){
                return $next($request,$response);
            }
            else{
                $respuesta = "No tienes permiso para realizar esta accion.";
                $newResponse = $response->withJson($respuesta,200);
                return $newResponse;
            }
        }

    }
?>