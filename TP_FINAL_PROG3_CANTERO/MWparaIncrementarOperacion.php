<?php
require_once "AutentificadorJWT.php";
class MWparaIncrementarOperacion
{
    ///Suma una operación al empleado.
    public static function IncrementarOperacionAEmpleado($request, $response, $next)
    {
        $arrayConToken = $request->getHeader('token');
        $token=$arrayConToken[0];
        $payload = AutentificadorJWT::ObtenerPayLoad($token);
        Empleado::IncrementarOperacion($payload->datos->id);
        return $next($request, $response);
    }
}
?>