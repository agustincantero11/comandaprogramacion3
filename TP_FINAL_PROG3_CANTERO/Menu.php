<?php

class Menu
{
    public $id;
    public $precio;
    public $nombre;
    public $sector;
    
    public static function Mostrar()
    {
        try {
            $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso();

            $consulta = $objetoAccesoDato->RetornarConsulta("SELECT m.id, m.nombre, m.precio, te.Descripcion as sector FROM menu m INNER JOIN 
                                                        tipoempleado te ON te.ID_tipo_empleado = m.id_sector;");

            $consulta->execute();

            $resultado = $consulta->fetchAll(PDO::FETCH_CLASS, "Menu");
        } catch (Exception $e) {
            $resultado = $e->getMessage();
        }
        finally {
            return $resultado;
        }
    }
    
    public static function Cargar($nombre, $precio, $sector)
    {
        $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso();
        $respuesta = "";
        try {
            $consulta = $objetoAccesoDato->RetornarConsulta("SELECT ID_tipo_empleado FROM tipoempleado WHERE Descripcion = :sector AND Estado = 'A';");

            $consulta->bindValue(':sector', $sector, PDO::PARAM_STR);
            $consulta->execute();
            $id_sector = $consulta->fetch();

            if ($id_sector != null) {
                $consulta = $objetoAccesoDato->RetornarConsulta("INSERT INTO menu (nombre, precio, id_sector) 
                                                                VALUES (:nombre, :precio, :id_sector);");

                $consulta->bindValue(':nombre', $nombre, PDO::PARAM_STR);
                $consulta->bindValue(':precio', $precio, PDO::PARAM_INT);
                $consulta->bindValue(':id_sector', $id_sector[0], PDO::PARAM_INT);

                $consulta->execute();

                $respuesta = "Registrado correctamente.";
            } else {
                $respuesta = "Debe ingresar un sector valido";
            }
        } catch (Exception $e) {
            $respuesta = $e->getMessage();
        }
        finally {
            return $respuesta;
        }
    }
    
    public static function Modificar($id, $nombre, $precio, $sector)
    {
        $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso();
        $respuesta = "";
        try {
            $consulta = $objetoAccesoDato->RetornarConsulta("SELECT ID_tipo_empleado FROM tipoempleado WHERE Descripcion = :sector AND Estado = 'A';");

            $consulta->bindValue(':sector', $sector, PDO::PARAM_STR);
            $consulta->execute();
            $id_sector = $consulta->fetch();

            if ($id_sector != null) {
                $consulta = $objetoAccesoDato->RetornarConsulta("UPDATE menu SET nombre = :nombre, precio = :precio, id_sector = :id_sector
                                                                WHERE id = :id;");

                $consulta->bindValue(':nombre', $nombre, PDO::PARAM_STR);
                $consulta->bindValue(':precio', $precio, PDO::PARAM_INT);
                $consulta->bindValue(':id', $id, PDO::PARAM_INT);
                $consulta->bindValue(':id_sector', $id_sector[0], PDO::PARAM_INT);

                $consulta->execute();

                $respuesta = "Modificado correctamente.";
            } else {
                $respuesta = "Debe ingresar un sector valido";
            }
        } catch (Exception $e) {
            $respuesta = $e->getMessage();
        }
        finally {
            return $respuesta;
        }
    }

    public static function Borrar($id)
    {
        try {
            $objetoAccesoDato = AccesoDatos::dameUnObjetoAcceso();

            $consulta = $objetoAccesoDato->RetornarConsulta("DELETE FROM menu WHERE id = :id");

            $consulta->bindValue(':id', $id, PDO::PARAM_INT);

            $consulta->execute();

            $respuesta = "Eliminado correctamente.";
        } catch (Exception $e) {
            $respuesta = $e->getMessage();
        }
        finally {
            return $respuesta;
        }
    }
}

?>