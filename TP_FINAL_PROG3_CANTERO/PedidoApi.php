<?php

include_once("Pedido.php");
include_once("AutentificadorJWT.php");


class PedidoApi {  
    
    public function TraerPorMesa($request,$response,$args){
        $mesa = $args["codigo"];  
        $respuesta = Pedido::TraerPorMesa($mesa);
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function CargarPedido($request, $response, $args){
        $parametros = $request->getParsedBody();
        foreach ($parametros as &$valor) {
            $id_mesa = $valor["id_mesa"];        
            $id_menu  = $valor["id_menu"];
            $nombre_cliente = $valor["cliente"];
            $arrayConToken = $request->getHeader('token');
            $token=$arrayConToken[0];
            $payload = AutentificadorJWT::ObtenerPayLoad($token);
            $id_mozo = $payload->datos->id;
            $respuesta[] = Pedido::Cargar($id_mesa,$id_menu,$id_mozo,$nombre_cliente);
        }
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function CancelarPedido($request,$response,$args){
        $parametros = $request->getParsedBody();
        $codigo = $parametros["codigo"];
        $respuesta = Pedido::Cancelar($codigo);
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function TraerTodosLosPedidos($request,$response,$args){
        $respuesta = Pedido::TraerTodos();
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function TraerTodosLosPedidosCancelados($request,$response,$args){
        $respuesta = Pedido::TraerCancelados();
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function TraerTodosLosPedidosPorFecha($request,$response,$args){
        $parametros = $request->getParsedBody();
        $fecha = $parametros["fecha"];  
        $respuesta = Pedido::TraerTodosPorFecha($fecha);
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function TomarPedidoPendiente($request,$response,$args){
        $parametros = $request->getParsedBody();
        $codigo = $parametros["codigo"];  
        $minutosEstimados = $parametros["minutosEstimados"];  
        $arrayConToken = $request->getHeader('token');
        $token=$arrayConToken[0];
        $payload = AutentificadorJWT::ObtenerPayLoad($token);
        $id_encargado = $payload->datos->id;
        $puesto = $payload->datos->puesto;
        $respuesta = Pedido::TomarPedido($codigo,$id_encargado,$minutosEstimados,$puesto);
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function InformarPedidoListoParaServir($request,$response,$args){
        $arrayConToken = $request->getHeader('token');
        $token=$arrayConToken[0];
        $payload = AutentificadorJWT::ObtenerPayLoad($token);
        $id_encargado = $payload->datos->id;
        $parametros = $request->getParsedBody();
        $codigo = $parametros["codigo"];  
        $respuesta = Pedido::InformarListoParaServir($codigo, $id_encargado);
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function ServirPedido($request,$response,$args){
        $arrayConToken = $request->getHeader('token');
        $token=$arrayConToken[0];
        $payload = AutentificadorJWT::ObtenerPayLoad($token);
        $parametros = $request->getParsedBody();
        $arrayConToken = $request->getHeader('token');
        $token=$arrayConToken[0];
        $payload = AutentificadorJWT::ObtenerPayLoad($token);
        $id_mozo = $payload->datos->id;
        foreach ($parametros as &$valor) {
            $codigo = $valor["codigo"]; 
            $respuesta[] = Pedido::Servir($codigo, $id_mozo);
        }
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function TiempoRestantePedido($request,$response,$args){
        $codigo = $args["codigoPedido"];
        $respuesta = Pedido::TiempoRestante($codigo);
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function ListarPedidosFueraDelTiempoEstipulado($request,$response,$args){
        $respuesta = Pedido::ListarFueraDelTiempoEstipulado();
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function LoMasVendido($request,$response,$args){
        $respuesta = Pedido::MasVendido();
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function LoMenosVendido($request,$response,$args){
        $respuesta = Pedido::MenosVendido();
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
}


?>