<?php

include_once("Mesa.php");
include_once("Foto.php");

class MesaApi {  
    
    public static function CargarMesa($request, $response, $args){
        
        $parametros = $request->getParsedBody();
        $codigo = $parametros["codigo"];            
       
        $respuesta = Mesa::CargarMesa($codigo);
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function TraerTodas($request,$response,$args){
        $respuesta = Mesa::TraerTodas();
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function BorrarMesa($request,$response,$args){
        $codigo = $args["codigo"];
        $respuesta = Mesa::Borrar($codigo);
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function SubirFotoMesa($request, $response, $args){
        $parametros = $request->getParsedBody();
        $files = $request->getUploadedFiles();
        $codigoMesa = $parametros["codigo"];
        $foto = $files["foto"];
 
        $ext = Foto::ObtenerExtension($foto);
        if($ext != "ERROR"){
            $rutaFoto = "./fotos/mesas/".$codigoMesa.$ext;
            Foto::GuardarFoto($foto,$rutaFoto);

            $respuesta = Mesa::SubirFoto($rutaFoto,$codigoMesa);
            $newResponse = $response->withJson($respuesta,200);
            return $newResponse;
        }
        else{
            $respuesta = "Ocurrio un error.";
            $newResponse = $response->withJson($respuesta,200);
            return $newResponse;
        }        
    }
    
    public function CambiarAEsperandoPedido($request,$response,$args){
        $parametros = $request->getParsedBody();
        $codigo = $parametros["codigo"]; 
        $respuesta = Mesa::CambiarAEsperandoPedido($codigo);
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function CambiarAComiendo($request,$response,$args){
        $parametros = $request->getParsedBody();
        $codigo = $parametros["codigo"]; 
        $respuesta = Mesa::CambiarAComiendo($codigo);
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function CambiarAPagando($request,$response,$args){
        $parametros = $request->getParsedBody();
        $codigo = $parametros["codigo"]; 
        $respuesta = Mesa::CambiarAPagando($codigo);
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function CerrarMesa($request,$response,$args){
        $parametros = $request->getParsedBody();
        $codigo = $parametros["codigo"]; 
        $respuesta = Mesa::CerrarMesa($codigo);
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function CobrarMesa($request,$response,$args){
        $parametros = $request->getParsedBody();
        $codigo = $parametros["codigo"];
        $respuesta = Mesa::Cobrar($codigo);
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function MesaMasUsada($request,$response,$args){
        $respuesta = Mesa::MasUsada();
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function MesaMenosUsada($request,$response,$args){
        $respuesta = Mesa::MenosUsada();
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function MesaMasFacturacion($request,$response,$args){
        $respuesta = Mesa::MasFacturacion();
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function MesaMenosFacturacion($request,$response,$args){
        $respuesta = Mesa::MenosFacturacion();
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }

    public function FacturaConMasImporte($request,$response,$args){
        $respuesta = Mesa::FacturaConMasImporte();
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function FacturaConMenosImporte($request,$response,$args){
        $respuesta = Mesa::FacturaConMenosImporte();
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function MesaConMejorPuntuacion($request,$response,$args){
        $respuesta = Mesa::MesaConMejorPuntuacion();
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function MesaConPeorPuntuacion($request,$response,$args){
        $respuesta = Mesa::MesaConPeorPuntuacion();
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
    
    public function FacturacionEntreFechas($request,$response,$args){
        $parametros = $request->getParsedBody();
        $codigoMesa = $parametros["codigo"];
        $fecha1 = $parametros["fecha1"];
        $fecha2 = $parametros["fecha2"];
        $respuesta = Mesa::FacturacionEntreFechas($codigoMesa,$fecha1,$fecha2);
        $newResponse = $response->withJson($respuesta,200);
        return $newResponse;
    }
}