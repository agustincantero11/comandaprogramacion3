<?php
require_once 'Empleado.php';
require_once 'IApiUsable.php';

class EmpleadoApi extends Empleado implements IApiUsable
{
 	public function TraerUno($request, $response, $args) {
     	$id=$args['id'];
    	$empleado=Empleado::TraerUnEmpleado($id);
     	$newResponse = $response->withJson($empleado, 200);  
    	return $newResponse;
    }
    
     public function TraerTodos($request, $response, $args) {
        $empleado = Empleado::TraerTodosLosEmpleados();

     	$newResponse = $response->withJson($empleado, 200);
    	return $newResponse;
    }
    
    public function CargarUno($request, $response, $args) {
			$ArrayDeParametros = $request->getParsedBody();
			$nombre= $ArrayDeParametros['nombre'];
			$apellido= $ArrayDeParametros['apellido'];
			$puesto= $ArrayDeParametros['puesto'];
			$email = $ArrayDeParametros['email'];
			$password = $ArrayDeParametros['password'];
			$empleado = new Empleado();
			$empleado->setNombre($nombre);
			$empleado->setApellido($apellido);
			$empleado->setPuesto($puesto);
			$empleado->setEmail($email);
			$empleado->setPassword($password);
			$respuesta = $empleado->InsertarElEmpleado();
			
			$response->getBody()->write($respuesta);

			return $response;  
    }

      public function BorrarUno($request, $response, $args) {
     	$ArrayDeParametros = $request->getParsedBody();
     	$id=$ArrayDeParametros['id'];
     	$empleado= new Empleado();
     	$empleado->id=$id;
     	$cantidadDeBorrados=$empleado->BorrarEmpleado();

     	$objDelaRespuesta= new stdclass();
	    $objDelaRespuesta->cantidad=$cantidadDeBorrados;
	    if($cantidadDeBorrados>0)
	    	{
	    		 $objDelaRespuesta->resultado="se borraron ".$cantidadDeBorrados." empleados.";
	    	}
	    	else
	    	{
	    		$objDelaRespuesta->resultado="no se borraron empleados!!!";
	    	}
	    $newResponse = $response->withJson($objDelaRespuesta, 200);  
      	return $newResponse;
    }
    
    public static function SuspenderEmpleado($request, $response, $args)
    {
		$ArrayDeParametros = $request->getParsedBody();
		$id=$ArrayDeParametros['id'];
		$empleado= new Empleado();
		$empleado->id=$id;
		
		$filasAfectadas=$empleado->Suspender();

     	$objDelaRespuesta= new stdclass();
	    if($filasAfectadas>0)
	    	{
	    		 $objDelaRespuesta->resultado="Empleado suspendido correctamente";
	    	}
	    	else
	    	{
	    		$objDelaRespuesta->resultado="El id es incorrecto o el usuario ya se encuentra suspendido!!!";
	    	}
	    return $response->withJson($objDelaRespuesta, 200);  
	   
    }
    
    public function ModificarUno($request, $response, $args) {

			$ArrayDeParametros = $request->getParsedBody();
	
			$id = $ArrayDeParametros['id'];
			$nombre = $ArrayDeParametros['nombre'];
			$apellido = $ArrayDeParametros['apellido'];
			$puesto = $ArrayDeParametros['puesto'];
			$email = $ArrayDeParametros['email'];
			$password = $ArrayDeParametros['password'];
	
			$empleado = Empleado::TraerUnEmpleado($id);

			if($empleado){
				if($nombre != "" || $nombre != null){
						$empleado->setNombre($nombre);
				}
				if($apellido != "" || $apellido != null){
						$empleado->setApellido($apellido);
				}
				if($puesto != "" || $puesto != null){
								$empleado->setPuesto($puesto);
				}

				if($email != "" || $email != null){
						$empleado->setEmail($email);
				}
				if($password != "" || $password != null){
						$empleado->setPassword($password);
				}
				$resultado = $empleado->ModificarEmpleado($id);

				if($resultado>0){
					$respuesta = "Empleado modificado con exito";
				}else{
					$respuesta = "No se realizaron modificaciones";
				}
				$objDelaRespuesta= new stdclass();
				$objDelaRespuesta->resultado=$respuesta;
				return $response->withJson($objDelaRespuesta, 200);	
				}else{
					return $response->withJson("El id ingresado es incorrecto", 200);	
				}
	}
    
    public static function ObtenerOperacionesPorPuesto($request, $response, $args)
    {
        $respuesta = Empleado::OperacionesPorPuesto();
      
        $newResponse = $response->withJson($respuesta, 200);
        return $newResponse;
    }
    
    public static function ObtenerOperacionesDeEmpleadosPorPuesto($request, $response, $args)
	{
		$puesto = $args['puesto'];
        $respuesta = Empleado::OperacionesDeEmpleadosPorPuesto($puesto);
        $newResponse = $response->withJson($respuesta, 200);
        return $newResponse;
	}
		
	public function ListarEmpleadosEntreFechasLogin($request,$response,$args){
			
        $parametros = $request->getParsedBody();
        $fecha1 = $parametros["fecha1"];
        $fecha2 = $parametros["fecha2"];
        $respuesta = Empleado::ListarEntreFechasLogin($fecha1,$fecha2);
        $newResponse = $response->withJson($respuesta,200);
		return  $newResponse;
    } 
    
    public function ListarEmpleadosEntreFechasRegistro($request,$response,$args){
			
		$parametros = $request->getParsedBody();
		$fecha1 = $parametros["fecha1"];
		$fecha2 = $parametros["fecha2"];
		$respuesta = Empleado::ListarEntreFechasRegistro($fecha1,$fecha2);
		$newResponse = $response->withJson($respuesta,200);
	    return  $newResponse;
	} 
}